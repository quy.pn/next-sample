import * as React from "react";

export const FlagIcon = () => (
  <svg
    width="15"
    height="18"
    viewBox="0 0 15 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M1 5.13159L7.91561 17.1098"
      stroke="#3D3D3D"
      stroke-linejoin="round"
    />
    <path
      d="M1 5.13159L11.9839 3.62973L5.19128 12.3911L1 5.13159Z"
      fill="#E70012"
      stroke="#3D3D3D"
      stroke-linejoin="round"
    />
  </svg>
);
