import * as React from "react";

export const SearchIcon = () => (
  <svg
    width="14"
    height="14"
    viewBox="0 0 14 14"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M6.22493 11.3166C9.03697 11.3166 11.3166 9.03697 11.3166 6.22493C11.3166 3.4129 9.03697 1.1333 6.22493 1.1333C3.4129 1.1333 1.1333 3.4129 1.1333 6.22493C1.1333 9.03697 3.4129 11.3166 6.22493 11.3166Z"
      stroke="#3D3D3D"
      stroke-miterlimit="10"
    />
    <path
      d="M9.82422 9.82397L13.0002 13"
      stroke="#3D3D3D"
      stroke-miterlimit="10"
    />
  </svg>
);
