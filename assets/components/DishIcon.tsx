import * as React from "react";

export const DishIcon = () => (
  <svg
    width="33"
    height="24"
    viewBox="0 0 33 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g clip-path="url(#clip0_2090_16601)">
      <path
        d="M0.342773 0.342773V7.43992L4.20134 10.1828L8.05305 7.43992V0.342773"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M4.20128 10.1829L2.83057 23.6571H5.572L4.20128 10.1829Z"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M31.9718 15.3736L30.6011 23.6571H32.6572V15.3736V0.342773L30.6902 3.89477L29.2715 11.9451L31.9718 15.3736Z"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M18.0867 20.2697C22.6516 20.2697 26.3521 16.5672 26.3521 11.9999C26.3521 7.4327 22.6516 3.73022 18.0867 3.73022C13.5218 3.73022 9.82129 7.4327 9.82129 11.9999C9.82129 16.5672 13.5218 20.2697 18.0867 20.2697Z"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M18.0866 17.7531C21.2623 17.7531 23.8367 15.1773 23.8367 12C23.8367 8.8226 21.2623 6.24683 18.0866 6.24683C14.9109 6.24683 12.3364 8.8226 12.3364 12C12.3364 15.1773 14.9109 17.7531 18.0866 17.7531Z"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M0.342773 7.43994H8.05305"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M2.83057 7.43992V0.342773"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
      <path
        d="M5.57178 7.43992V0.342773"
        stroke="#3D3D3D"
        stroke-linejoin="round"
      />
    </g>
    <defs>
      <clipPath id="clip0_2090_16601">
        <rect width="33" height="24" fill="white" />
      </clipPath>
    </defs>
  </svg>
);
