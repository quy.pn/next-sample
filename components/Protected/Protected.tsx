import { FC, useState } from "react";
import styled from "styled-components";

interface ProtectedProps {
  onSubmit: (v: string) => void;
}

export const Protected: FC<ProtectedProps> = ({ onSubmit }: ProtectedProps) => {
  const [text, setText] = useState<string>("");

  return (
    <ProtectedWrapper>
      <Input value={text} onChange={(e) => setText(e.target.value)} type="password" placeholder="Enter passcode" />
      <Button disabled={!text} onClick={() => onSubmit(text)}>Submit</Button>
    </ProtectedWrapper>
  )
}

const ProtectedWrapper = styled.div `
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  display: flex;
  position: fixed;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`

const Button = styled.button `
  border: 0;
  height: 40px;
  width: 256px;
  padding: 0 20px;
  border-radius: 40px;
  cursor: pointer;
  margin-top: 20px;
  color: #000000;

  &:hover {
    background-color: #bfbfbf;
  }
`

const Input = styled.input `
  height: 40px;
  width: 256px;
  padding: 0 20px;
  border-radius: 40px;
  border: 1px solid ${({ theme }) => theme.colors.inputBorder};
`
