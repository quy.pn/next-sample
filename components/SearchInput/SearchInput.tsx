import { FC, ReactNode } from "react";
import styled from "styled-components";
import { SearchIcon } from "../icons";

export interface SearchInputProps {
  query?: string;
}

export const SearchInput: FC<SearchInputProps> = ({ query, ...props }: SearchInputProps) => {
  return (
    <SearchWrapper {...props}>
      <SearchIcon />
      <div className="divider" />
      <Input placeholder="キーワード検索" />
    </SearchWrapper>
  )
}

const SearchWrapper = styled.div `
  height: 40px;
  margin-top: 13px;
  position: relative;
  box-sizing: border-box;

  svg {
    top: 13px;
    left: 14px;
    z-index: 2;
    position: absolute;
  }

  .divider {
    z-index: 3;
    width: 1px;
    height: 12px;
    right: 0;
    left: 37px;
    top: 14px;
    position: absolute;
    background-color: ${({ theme }) => theme.colors.border};
  }
`

const Input = styled.input `
  height: 40px;
  width: 256px;
  padding-left: 46px;
  border-radius: 40px;
  border: 1px solid ${({ theme }) => theme.colors.inputBorder};
  font-size: ${({ theme }) => theme.sizes.text}px;
`
