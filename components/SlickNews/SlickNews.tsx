import React, { FC } from "react";
import Slider from "react-slick";
import Link from "next/link";
import dayjs from "dayjs";
import styled from "styled-components";
import { ArrowLeftIcon, ArrowRightIcon } from "../icons";

export interface SlickItemProps {
  title: string;
  category: string;
  createdAt: string;
  startDate?: string;
  thumb?: string;
}

export interface SlickNewsProps {
  items: Array<SlickItemProps>;
}

const NextArrow = (props: any) => {
  return (
    <FloatButton {...props}>
      <ArrowRightIcon />
    </FloatButton>
  );
};

const PreArrow = (props: any) => {
  return (
    <FloatButton {...props}>
      <ArrowLeftIcon />
    </FloatButton>
  );
};

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 3,
  initialSlide: 0,
  nextArrow: <NextArrow />,
  prevArrow: <PreArrow />,
  responsive: [
    {
      breakpoint: 1300,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};

export const SlickNews: FC<SlickNewsProps> = ({
  items,
  ...props
}: SlickNewsProps) => {
  return (
    <SliderList {...settings} {...props}>
      {items.map((item: SlickItemProps, index: number) => (
        <SlickItem key={item.title} className={`item-slick-${index + 1}`}>
          <Link href="#">
            <SlickThumb src={item.thumb} />
          </Link>
          <SlickBody>
            <SlickHeader>
              <Link href="#">
                <Category>{item.category}</Category>
              </Link>
              <SlickTime>
                {dayjs(item.startDate).format("YYYY.MM.DD")}
              </SlickTime>
            </SlickHeader>
            <Link href="#">
              <Title>{item.title}</Title>
            </Link>
          </SlickBody>
        </SlickItem>
      ))}
    </SliderList>
  );
};

const FloatButton = styled.button`
  border: 0;
  z-index: 10;
  top: calc(50% - 13px);
  left: -3px;
  width: 30px;
  height: 30px;
  cursor: pointer;
  position: absolute;
  text-align: center;
  background: transparent;

  &.slick-next {
    right: 0;
    left: initial;
    padding-left: 10px;
  }
`;

const SliderList = styled(Slider)`
  padding-top: 2px;
  position: relative;

  @media (max-width: 768px) {
    padding: 0 30px;
    margin-top: 20px;
    margin-bottom: 17px;
  }

  ${({ theme }) => theme.breakpoints.mddown} {
    // padding: 0;
  }
`;

const SlickThumb = styled.img`
  width: 75px;
  height: 75px;
  margin-top: 1px;
  margin-right: 20px;
  border-radius: 10px;
`;

const SlickTime = styled.div`
  font-family: "Noto Sans JP";
  color: ${({ theme }) => theme.colors.timer};
  font-size: ${({ theme }) => theme.sizes.mall}px;
  font-weight: 400;
  text-align: right;

  @media (max-width: 768px) {
    font-weight: 500;
  }
`;

const SlickHeader = styled.div`
  display: flex;
  padding: 0 0 10px 0;
  justify-content: space-between;
`;

const SlickBody = styled.div`
  display: flex;
  flex-direction: column;
`;

const Category = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px 10px;
  border-radius: 30px;
  line-height: 140%;
  color: ${({ theme }) => theme.colors.light};
  font-size: ${({ theme }) => theme.sizes.thin}px;
  background-color: ${({ theme }) => theme.colors.bg};
  font-family: "Noto Sans JP";
  font-weight: 400;
`;

const Title = styled.div`
  font-family: "Noto Sans JP";
  text-decoration: underline;
  font-weight: 700;
  line-height: 160%;
  color: ${({ theme }) => theme.colors.bg};
  font-size: ${({ theme }) => theme.sizes.normal}px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2; /* number of lines to show */
  line-clamp: 2;
  -webkit-box-orient: vertical;
  margin-top: -1px;
  padding-right: 3px;
`;

const SlickItem = styled.div`
  display: flex !important;
  position: relative;
  align-items: center;

  ${({ theme }) => theme.breakpoints.md} {
    padding: 20px;
    padding-right: 15px;
    margin-left: 15px;

    &.item-slick-3 {
      margin-left: 28px;
      margin-top: 2px;
      padding-right: 30px;
    }

    &.item-slick-1 {
      margin: 2px 0px 0px 0px;
      max-width: 364px;

      &::before {
        display: none;
      }
    }

    &.item-slick-2 {
      max-width: 364px;
      margin: 2px -11px 0px 15px;
    }
  }

  &::before {
    content: "";
    top: 20px;
    left: -8px;
    bottom: 0;
    right: 20px;
    width: 1px;
    height: 75px;
    position: absolute;
    background-color: ${({ theme }) => theme.colors.lightGray};
  }

  @media (max-width: 768px) {
    max-width: initial;

    &::before {
      display: none;
    }
  }
`;
