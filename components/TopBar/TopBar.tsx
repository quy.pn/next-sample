import { FC } from "react";
import styled from "styled-components";

export interface TopbarItemProps {
  title: string;
  icon?: any;
  items?: Array<TopbarItemProps>;
}

export interface TopBarProps {
  items: Array<TopbarItemProps>;
}

export const TopBar: FC<TopBarProps> = ({ items, ...props }: TopBarProps) => {
  return (
    <NavList {...props}>
      {items.map((item: TopbarItemProps) => (
        <TopbarItem key={item.title}>
          {item.icon}
          <TopbarItemTitle>
            {item.title}
          </TopbarItemTitle>
        </TopbarItem>
      ))}
    </NavList>
  )
}

const NavList = styled.div `
  display: flex;
  right: 0;
  width: 100%;
  height: 58px;
  // position: absolute;
  border-radius: 0px 0px 0px 12px;
  border: 1px solid ${({ theme }) => theme.colors.inderBorder};
  border-top: 0;
  border-right: 0;
  font-weight: 700;
  font-size: ${({ theme }) => theme.sizes.snormal}px;
`

const TopbarItem = styled.div `
  display: flex;
  min-width: 116px;
  padding: 14px 16px;
  align-items: center;
  position: relative;

  svg {
    margin: 7px;
    margin-left: 2px;
  }

  &::after {
    width: 1px;
    height: 12px;
    content: "";
    right: 2px;
    top: calc(50% - 6px);
    position: absolute;
    background-color: ${({ theme }) => theme.colors.border};
  }

  &:last-child {
    padding-left: 15px;

    &::after {
      display: none;
    }

    svg {
      margin-right: 9px;
    }
  }
`

const TopbarItemTitle = styled.div `
`
