export const MenuIcon = (props: any) => (
  <svg width="17" height="15" viewBox="0 0 17 15" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M0 1H17" stroke="white"/>
    <path d="M0 7H17" stroke="white"/>
    <path d="M0 14H17" stroke="white"/>
  </svg>
)
