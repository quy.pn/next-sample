export const FloorIcon = (props: any) => (
  <svg width="32" height="24" viewBox="0 0 32 24" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <g clipPath="url(#clip0_1562_11766)">
      <path d="M9.43055 15.165L1 18.3609L15.9861 23.6372L31 18.3609L22.5556 15.165" stroke="#3D3D3D" strokeMiterlimit="10" />
      <path d="M9.43055 9.0061L1 12.202L15.9861 17.4783L31 12.202L22.5556 9.0061" stroke="#3D3D3D" strokeMiterlimit="10" />
      <path d="M15.9861 0.369629L1 6.04285L15.9861 11.3192L31 6.04285L15.9861 0.369629Z" stroke="#3D3D3D" strokeMiterlimit="10" />
    </g>
    <defs>
      <clipPath id="clip0_1562_11766">
        <rect width="32" height="24" fill="white" />
      </clipPath>
    </defs>
  </svg>

)