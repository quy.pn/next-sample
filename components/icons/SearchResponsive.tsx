export const SearchResponsiveIcon = ({ shadow = true, ...props }: any) => (
  <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M7.86503 14.7301C11.6565 14.7301 14.7301 11.6565 14.7301 7.86503C14.7301 4.07358 11.6565 1 7.86503 1C4.07358 1 1 4.07358 1 7.86503C1 11.6565 4.07358 14.7301 7.86503 14.7301Z" stroke="#3D3D3D" strokeMiterlimit="10" />
    <path d="M12.7178 12.7175L17 16.9998" stroke="#3D3D3D" strokeMiterlimit="10" />
  </svg>
)