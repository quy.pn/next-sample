export const Dash = (props: any) => (
  <svg
    width="6"
    height="24"
    viewBox="0 0 6 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    style={{ marginLeft: 10, marginRight: 3 }}
    {...props}
  >
    <path d="M5 1L1.5 23" stroke="#3D3D3D" strokeWidth="2" />
  </svg>
);
