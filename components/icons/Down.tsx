export const DownIcon = (props: any) => (
<svg
  width={13}
  height={8}
  viewBox="0 0 13 8"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
  {...props}
>
  <path d="M1 1L6.5 6.5L12 1" stroke="#3D3D3D" />
</svg>
)

export const ArrowLeftIcon = (props: any) => (
<svg
  width={7}
  height={12}
  viewBox="0 0 7 12"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
  {...props}
>
  <path d="M6 11L1 6L6 1" stroke="#3D3D3D" />
</svg>
)

export const ArrowRightIcon = (props: any) => (
<svg
  width={7}
  height={12}
  viewBox="0 0 7 12"
  fill="none"
  xmlns="http://www.w3.org/2000/svg"
  {...props}
>
  <path d="M1 1L6 6L1 11" stroke="#3D3D3D" />
</svg>
)