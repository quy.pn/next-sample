import { FC, ReactNode } from "react";
import styled from "styled-components";
import { DownIcon } from "../icons";

export interface NavItemProps {
  title: string | ReactNode;
  subTitle: string;
  icon?: any;
  items?: Array<NavItemProps>;
}

export interface NavProps {
  items: Array<NavItemProps>;
}

export const Navigation: FC<NavProps> = ({ items, ...props }: NavProps) => {
  return (
    <NavList {...props}>
      {items.map((item: NavItemProps) => (
        <NavItem key={`${item.title}` + item.subTitle}>
          <NavIcon>{item.icon}</NavIcon>
          <NavItemTitle>{item.title}</NavItemTitle>
          <div className="subtitle-icon">
            <NavItemSubtitle>{item.subTitle}</NavItemSubtitle>
            <span style={{ display: "block", marginTop: -5 }}>
              <DownIcon />
            </span>
          </div>
        </NavItem>
      ))}
    </NavList>
  );
};

const NavItemTitle = styled.div`
  font-weight: 700;
  line-height: 22.4px;
  font-size: ${({ theme }) => theme.sizes.heading}px;
`;

const NavItemSubtitle = styled.div`
  padding-bottom: 0;
  font-weight: 500;
  line-height: 16px;
  color: ${({ theme }) => theme.colors.caption};
  font-size: ${({ theme }) => theme.sizes.small}px;
`;

const NavIcon = styled.div`
  display: flex;
  padding: 5px 9px 8px 10px;
  flex-direction: column;
  justify-content: center;
`;

const NavItem = styled.div`
  display: flex;
  cursor: pointer;
  text-align: center;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: relative;
  color: ${({ theme }) => theme.colors.text};

  .subtitle-icon {
    margin-top: 2px;
  }

  &:hover {
    svg {
      transform: scale(1.2);
      transition: all 0.5s ease;
    }

    ${NavItemSubtitle} {
      color: ${({ theme }) => theme.colors.text};
    }
  }

  svg {
    transition: all 0.5s ease;
  }

  &::after {
    width: 1px;
    height: 24px;
    content: "";
    right: -3px;
    top: calc(50% - 20px);
    position: absolute;
    background-color: ${({ theme }) => theme.colors.border};
  }

  &:last-child {
    &::after {
      display: none;
    }
  }
`;

const NavList = styled.div`
  flex: 1;
  display: grid;
  margin: auto 0;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;

  @media (min-width: 1400px) and (max-width: 1441px) {
    ${NavItem} {
      &:first-child {
        width: 160px;
        padding-top: 0;
        padding-left: 8px;

        ${NavIcon} {
          padding-top: 9px;
          padding-bottom: 9px;
        }

        &::after {
          right: -1px;
        }
      }

      &:nth-child(2) {
        width: 150px;

        ${NavIcon} {
          padding: 11px 8px 10px 10px;
        }

        &::after {
          right: -13px;
        }
      }

      &:nth-child(3) {
        width: 158px;

        &::after {
          right: -8px;
        }

        ${NavIcon} {
          padding: 7px 9px 10px 9px;
        }
      }

      &:nth-child(4) {
        width: 166px;
        ${NavIcon} {
          padding: 8px 6px 8px 5px;
        }
      }

      &:nth-child(5) {
        width: 166px;
        padding-left: 17px;

        ${NavIcon} {
          padding: 7px 6px 8px 6px;
        }
      }
    }
  }

  @media (min-width: 200px) and (max-width: 1280px) {
    ${NavItem} {
      padding: 5px;
      font-size: 16px;
      justify-content: flex-start;
    }

    ${NavItemTitle} {
      .break {
        height: 0;
      }

      span {
        display: block;
      }
    }
  }

  ${({ theme }) => theme.breakpoints.mddown} {
    left: 0;
    right: 0;
    bottom: 0;
    height: 76px;
    z-index: 99;
    position: fixed;
    background: ${({ theme }) => theme.colors.light};

    ${NavItemTitle} {
      font-size: 10px;
      line-height: 13px;

      .break {
        height: 0;
      }

      span {
        display: block;
      }
    }

    ${NavItem} {
      padding: 0 10px;
      justify-content: flex-start;

      &:first-child {
        ${NavIcon} {
          padding: 9px 0px 7px 0px;
        }
        ${NavItemTitle} {
          padding-right: 2px;
        }
      }
      &:nth-child(2) {
        ${NavIcon} {
          padding: 10px 2px 5px 2px;
        }
      }
      &:nth-child(3) {
        ${NavIcon} {
          padding: 8px 2px 3px 0px;
        }
      }
      &:nth-child(4) {
        ${NavIcon} {
          padding: 7px 0px 8px 0px;
        }
      }
      &:nth-child(5) {
        ${NavIcon} {
          padding: 8px 0px 2px 0px;
        }
      }

      &::after {
        top: 20px;
        left: 0px;
        height: 11px;
      }

      &:hover {
        svg {
          transform: scale(1);
          transition: all 0.5s ease;
        }
      }
    }

    ${NavIcon} {
      padding-bottom: 5px;
    }

    svg {
      transform: scale(0.75);
    }

    .subtitle-icon {
      display: none;
    }
  }
`;
