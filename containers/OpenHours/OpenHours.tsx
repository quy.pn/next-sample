import React, { FC } from 'react';
import styled from 'styled-components';
import { RestaurantIcon, MealIcon, Dash } from '../../components/icons';

interface OpenHours {
  type: 'SHOP' | 'RESTAURANT';
  floor1: string;
  floor2: string;
  open: string;
  close: string;
  caption?: string;
}

interface OpenHoursProps {
  shop: OpenHours;
  restaurant: OpenHours;
}

export const OpenHours: FC<OpenHoursProps> = ({
  shop,
  restaurant,
}: OpenHoursProps) => {
  return (
    <SectionWrapper>
      <SectionHeader>
        8 <Dash style={{ margin: 0 }} /> 31<span>Wed.</span>
      </SectionHeader>
      <SectionBody>
        <SectionItem>
          <ItemIcon>
            <RestaurantIcon />
            <span>ショップ</span>
          </ItemIcon>
          <ItemBody>
            <ItemMeta>
              <ItemLabel>{shop.floor1}</ItemLabel>
              <ItemValue>
                {shop.open}〜{shop.close}
              </ItemValue>
            </ItemMeta>
            <ItemMeta>
              <ItemLabel>{shop.floor2}</ItemLabel>
              <ItemValue>{shop.caption}</ItemValue>
            </ItemMeta>
          </ItemBody>
        </SectionItem>
        <SectionItem>
          <ItemIcon>
            <MealIcon />
            <span className="meal">飲食店</span>
          </ItemIcon>
          <ItemBody>
            <ItemMeta>
              <ItemLabel>{restaurant.floor1}</ItemLabel>
              <ItemValue>
                {restaurant.open}〜{restaurant.close}
              </ItemValue>
            </ItemMeta>
            <ItemMeta>
              <ItemLabel>{restaurant.floor2}</ItemLabel>
              <ItemValue>{restaurant.caption}</ItemValue>
            </ItemMeta>
          </ItemBody>
        </SectionItem>
      </SectionBody>
    </SectionWrapper>
  );
};

const SectionWrapper = styled.div`
  order: 2;
  margin-top: 15px;
  padding: 16px 16px 0px 16px;

  max-width: 256px;
  border-radius: 20px;
  font-weight: 600;
  font-size: ${({ theme }) => theme.sizes.normal}px;
  background-color: ${({ theme }) => theme.colors.secondarybg};

  ${({ theme }) => theme.breakpoints.mddown} {
    max-width: 335px;
    margin: 7px auto;
    width: 100%;
  }
`;

const SectionHeader = styled.div`
  text-align: center;
  font-weight: 600;
  padding-left: 13px;
  font-size: ${({ theme }) => theme.sizes.large}px;

  span {
    font-size: ${({ theme }) => theme.sizes.normal}px;
  }
`;

const SectionBody = styled.div``;

const SectionItem = styled.div`
  display: flex;
  padding: 8px 0;
  padding-top: 16px;
  padding-left: 2px;
  padding-bottom: 20px;
  align-items: center;

  &:first-child {
    padding-top: 8px;
    padding-left: 0;
    border-bottom: 1px dashed;
  }
`;

const ItemBody = styled.div``;

const ItemMeta = styled.div`
  display: flex;
  margin-top: 6px;
  margin-right: 0;
  font-size: ${({ theme }) => theme.sizes.small}px;
`;

const ItemLabel = styled.div`
  height: 21px;
  min-width: 60px;
  text-align: center;
  border-radius: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.colors.light};
`;

const ItemValue = styled.div`
  height: 21px;
  display: flex;
  margin-left: 6px;
  align-items: center;
`;

const ItemIcon = styled.div`
  width: 46px;
  display: flex;
  align-items: center;
  flex-direction: column;
  font-size: ${({ theme }) => theme.sizes.small}px;

  .meal {
    margin-right: 7px;
    margin-top: 0px;
  }
`;
