import React, { FC } from 'react';
import styled from 'styled-components';
import { FlagIcon } from '../../components/icons/Flag';

export const Promotion: FC<{}> = () => {
  return (
    <SectionWrapper>
      <Image src="/assets/images/promotion.png" />
      <CardBody>
        <Category>
          <FlagIcon />
          <span>開催中のイベント</span>
        </Category>
        <CardTitle>リンクスるSALE2022 SUMMER開催☀️</CardTitle>
        <CardCaption>️開催期間：2022.07.20〜08.20</CardCaption>
      </CardBody>
    </SectionWrapper>
  );
};

const Image = styled.img`
  width: 100%;
`;

const Category = styled.div`
  padding-top: 6px;
  padding-bottom: 8px;
  font-weight: 600;
  line-height: 20px;
  border-bottom: 1px solid #cdcdcd;
  color: ${({ theme }) => theme.colors.highlight};
  font-size: ${({ theme }) => theme.sizes.text}px;

  span {
    padding: 0 2px;
  }

  ${({ theme }) => theme.breakpoints.mddown} {
    padding-top: 6px;
  }
`;

const CardBody = styled.div`
  padding: 16px 0;
  padding-top: 12px;
`;

const CardTitle = styled.div`
  font-weight: 600;
  font-size: 18px;
  padding: 16px 0;
  padding-bottom: 9px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2; /* number of lines to show */
  line-clamp: 2;
  // font-family: 'Noto Sans JP';
  -webkit-box-orient: vertical;
`;

const CardCaption = styled.div`
  font-size: 13px;
  color: #666666;
`;

const SectionWrapper = styled.div`
  order: 2;
  margin: 32px 0;

  ${({ theme }) => theme.breakpoints.mddown} {
    order: 1;
    display: flex;
    padding: 14px;
    margin: 7px 0;
    border-radius: 10px;
    border: 1px solid #cdcdcd;

    ${Image} {
      width: 150px;
      height: 150px;
      border-radius: 10px;
    }

    ${CardBody} {
      padding: 0 0 0 13px;
    }

    ${Category} {
      border: 0;
      padding-top: 6px;
      font-size: 14px;
    }

    ${CardTitle} {
      padding: 0;
      font-size: 16px;
      line-height: 160%;
    }
  }
`;
