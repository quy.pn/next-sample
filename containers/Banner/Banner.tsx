import React, { FC } from "react";
import styled from "styled-components";
import { Col, Row } from "../../components";
import { SlickItemProps, SlickNews } from "../../components/SlickNews";
import { OpenHours } from "../OpenHours/OpenHours";
import { Promotion } from "../Promotion";

const openhours: any = {
  shop: {
    floor1: "1F〜7F",
    floor2: "B1〜B2",
    open: "10:00",
    close: "20:00",
    caption: "店舗によって異なる",
  },
  restaurant: {
    floor1: "1F〜7F",
    floor2: "B1〜B2",
    open: "10:00",
    close: "20:00",
    caption: "店舗によって異なる",
  },
};

interface BannerProps {
  news: Array<SlickItemProps>
}

export const Banner: FC<BannerProps> = ({ news }: BannerProps) => {
  return (
    <BannerWrapper>
      <Row>
        <LeftSide xs>
          <BannerMain src="/assets/images/banner.png" />
          <SlickNews items={news} />
        </LeftSide>
        <RightSide>
          <OpenHours {...openhours} />
          <Promotion />
        </RightSide>
      </Row>
    </BannerWrapper>
  );
};

const LeftSide = styled(Col)`
  width: 100%;
  padding: 0px;
  max-width: calc(100% - 340px);

  ${({ theme }) => theme.breakpoints.mddown} {
    max-width: 100%;
  }
`;

const RightSide = styled(Col)`
  width: 100%;
  margin-left: 44px;
  max-width: 256px;
  display: flex;
  flex-direction: column;

  ${({ theme }) => theme.breakpoints.mddown} {
    padding: 0 10px;
    margin-left: 0;
    max-width: 100%;
  }
`;

const BannerMain = styled.img`
  width: 100%;
  margin-top: -3px;
  border-radius: 0px 20px 20px 0px;
  ${({ theme }) => theme.breakpoints.mddown} {
    margin-top: 0;
    border-radius: 0 0 0 0;
  }
`;

const BannerWrapper = styled.div `
  margin-right: -8px;
  margin-left: -8px;
`
