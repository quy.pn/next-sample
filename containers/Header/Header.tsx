import React, { FC } from "react";
import styled from "styled-components";
import Link from 'next/link'
import { useMediaQuery } from 'react-responsive'
import { Col, Navigation, NavItemProps, Row, TopBar, TopbarItemProps, SearchInput } from "../../components";
import * as Icons from "../../components/icons";
import { Alert } from "../Alert";
import { Mobilebar } from "./Mobilebar";

export const Header: FC = () => {
  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 1024px)'
  })

  const menus: Array<NavItemProps> = [
    {
      title: isDesktopOrLaptop ? '店舗を探す' : 'ショップ',
      subTitle: 'SHOP',
      icon: <Icons.HomeIcon />
    },
    {
      title: <span>フロア<span className="break"></span>マップ</span>,
      subTitle: 'FLOOR MAP',
      icon: <Icons.FloorIcon />
    },
    {
      title: <span>ニュース<span className="break">・</span>イベント</span>,
      subTitle: 'NEWS / EVENT',
      icon: <Icons.NewsIcon />
    },
    {
      title: '営業時間',
      subTitle: 'OPEN',
      icon: <Icons.ClockIcon />
    },
    {
      title: <span>アクセス<span className="break">・</span>駐車場</span>,
      subTitle: 'ACCESS',
      icon: <Icons.MapIcon />
    },
  ]

  const topbars: Array<TopbarItemProps> = [
    {
      title: '施設情報',
      icon: <Icons.WarnningIcon />
    },
    {
      title: 'FAQ・お問い合わせ',
      icon: <Icons.FAQIcon />
    },
  ]

  return (
    <HeaderWrapper>
      <Row>
        <Col xs>
          <Row>
            <LogoCell>
              <Link href="/">
                <Logo src="/assets/images/logo.png" />
              </Link>
              {!isDesktopOrLaptop && <Mobilebar />}
            </LogoCell>
            <Navigation items={menus} />
          </Row>
        </Col>
        <RightSide>
          <TopBar items={topbars} />
          <SearchInput />
        </RightSide>
        {!isDesktopOrLaptop && (
          <Col xs={12}>
            <Alert />
          </Col>
        )}
      </Row>
    </HeaderWrapper>
  )
}


const LogoCell = styled(Col)`
  ${({ theme }) => theme.breakpoints.mddown} {
    padding: 10px;
    width: 100%;
    display: flex;
    justify-content: space-between;
    padding-bottom: 6px;
  }
`

const Logo = styled.img`
  width: 180px;
  height: 69px;
  margin: 46px 22px 44px 48px;

  ${({ theme }) => theme.breakpoints.mddown} {
    width: 108px;
    height: 41px;
    margin: 10px 0;
  }
`

const RightSide = styled(Col)`
  width: 100%;
  padding-left: 44px;
  max-width: 340px;
  display: none;
  flex-wrap: wrap;

  ${({ theme }) => theme.breakpoints.md} {
    display: flexbox;
  }
`

const HeaderWrapper = styled.header`
  margin-right: -8px;
  margin-left: -8px;
`
