import React, { FC } from "react";
import styled from "styled-components";
import * as Icons from "../../components/icons";

export const Mobilebar: FC<{}> = () => {
  return (
    <MobilebarWrapper>
      <ButtonIcon>
        <Icons.SearchResponsiveIcon shadow={false} width={18} height={18} />
      </ButtonIcon>
      <ButtonIcon>
        <Icons.MapIcon width={15} height={17.56} />
      </ButtonIcon>
      <ButtonIcon isBlack>
        <Icons.MenuIcon />
      </ButtonIcon>
    </MobilebarWrapper>
  )
}

const MobilebarWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-right: -5px;
  margin-top: -5px;
`

const ButtonIcon = styled.button<{ isBlack?: boolean }> `
  margin: 5px;
  margin-top: 6px;
  width: 43px;
  height: 43px;
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  border-radius: 43px;
  padding-top: 4px;
  background-color: ${({ theme, isBlack }) => isBlack ? theme.colors.dark : theme.colors.light};
  border: 1px solid ${({ theme }) => theme.colors.lightGray};

  &:hover {
    border-color: ${({ theme }) => theme.colors.dark};
  }
`