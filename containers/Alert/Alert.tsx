import React, { FC } from "react";
import styled from "styled-components";

export const Alert: FC<{}> = () => {
  return (
    <AlertWrapper>
      【大切なお知らせ】新型コロナウイルス感染症(COVID‑19)対策による営業時間のお知らせ
    </AlertWrapper>
  );
};

const AlertWrapper = styled.div`
  padding: 16px;
  text-align: center;
  text-decoration: underline;
  margin: 0 ${({ theme }) => theme?.spaces?.alert}px;
  background-color: ${({ theme }) => theme?.colors.skin};
  font-size: ${({ theme }) => theme?.sizes?.caption ?? 15}px;
  font-family: "Noto Sans JP";
  font-weight: 700;
  ${({ theme }) => theme.breakpoints.mddown} {
    padding: 9px 10px 10px 13px;
    margin: 0;
    margin-bottom: 8px;
    font-weight: 700;
    font-size: 12px;
    line-height: 19.2px;
    text-align: left;
  }
`;
