import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }


  body {
    margin: 0;
    width: 100vw;
    overflow: hidden;
    overflow-y: scroll;
    font-family: 'Noto Sans JP','Google Sans Text',Arial,Helvetica,sans-serif;
  }

  :root {
    --gutter-width: 1rem;
    --outer-margin: 2rem;
    --gutter-compensation: calc((var(--gutter-width) * 0.5) * -1);
    --half-gutter-width: calc((var(--gutter-width) * 0.5));
    --xs-min: 30;
    --sm-min: 48;
    --md-min: 64;
    --lg-min: 75;
    --screen-xs-min: var(--xs-min)em;
    --screen-sm-min: var(--sm-min)em;
    --screen-md-min: var(--md-min)em;
    --screen-lg-min: var(--lg-min)em;
    --container-sm: calc(var(--sm-min) + var(--gutter-width));
    --container-md: calc(var(--md-min) + var(--gutter-width));
    --container-lg: calc(var(--lg-min) + var(--gutter-width));
  }

  @custom-media --sm-viewport only screen and (min-width: 48em);
  @custom-media --md-viewport only screen and (min-width: 64em);
  @custom-media --lg-viewport only screen and (min-width: 75em);
`;

export const theme = {
  colors: {
    primary: "#000000",
    secondary: "#F00000",
    text: "hsla(0, 0%, 24%, 1)",
    caption: "hsla(0, 0%, 67%, 1)",
    skin: "hsla(51, 47%, 94%, 1)",
    border: "hsla(0, 0%, 80%, 1)",
    inderBorder: "#3D3D3D",
    inputBorder: "#AAAAAA",
    bg: "#3D3D3D",
    primarybg: "#3D3D3D",
    secondarybg: "#F5F5F5",
    light: "#FFFFFF",
    dark: "#3D3D3D",
    lightGray: "#CDCDCD",
    highlight: "#F00E0E",
    timer: "#666666",
  },
  spaces: {
    alert: 25,
  },
  sizes: {
    text: 14,
    normal: 15,
    caption: 15,
    heading: 16,
    small: 11,
    mall: 12,
    snormal: 13,
    thin: 12,
    large: 27,
  },
  breakpoints: {
    mobile: "",
    sm: "@media (min-width: 48em)",
    md: "@media (min-width: 64em)",
    lg: "@media (min-width: 75em)",
    smdown: "@media (max-width: 48em)",
    mddown: "@media (max-width: 64em)",
    lgdown: "@media (max-width: 75em)",
  },
};
