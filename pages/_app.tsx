import 'react-flexbox-grid/dist/react-flexbox-grid.css'
import type { AppProps } from 'next/app'
// import { useState } from 'react';
import { useComponentHydrated } from "react-hydration-provider";
import { Context as ResponsiveContext } from "react-responsive";
import { ThemeProvider } from 'styled-components';
import { GlobalStyles, theme } from '../theme/theme';
import Head from 'next/head';

const HeadLink = () => (
  <Head>
    <title>Links Umeda</title>
    <meta name="description" content="" />
  </Head>
)

const Root = ({ Component, pageProps }: AppProps) => {
  const hydrated = useComponentHydrated();
  // const [light, setTheme] = useState<boolean>(true);

  if (hydrated) {
    return (
      <ThemeProvider theme={theme}>
        <HeadLink />
        <GlobalStyles />
        <Component {...pageProps} />
      </ThemeProvider>
    )
  }

  return (
    <ResponsiveContext.Provider value={{}}>
      <ThemeProvider theme={theme}>
        <HeadLink />
        <GlobalStyles />
        <Component {...pageProps} />
      </ThemeProvider>
    </ResponsiveContext.Provider>
  );
}

export default Root;
