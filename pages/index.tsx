import type { NextPage } from 'next'
import { useEffect, useState } from 'react';
import { useMediaQuery } from 'react-responsive';
import styled from 'styled-components';
import { Grid, Protected } from '../components';
import { Banner, Alert, Header, SlickItemProps } from '../containers'
import { axios } from '../utils/axios';

interface HomeProps {
  news: Array<SlickItemProps>
}

const PASSCODE = "123456";

const Home: NextPage<HomeProps> = ({ news }: HomeProps) => {
  const [isAuth, setAuth] = useState<boolean>(false);

  const isDesktopOrLaptop = useMediaQuery({
    query: '(min-width: 1024px)'
  })

  useEffect(() => {
    if (typeof window !== 'undefined' && localStorage.getItem("passcode") === PASSCODE) {
      setAuth(true);
    }
  }, []);

  if (!isAuth) {
    return <Protected onSubmit={(t) => {
      if (t === PASSCODE) {
        setAuth(true);
        if (typeof window !== 'undefined') {
          localStorage.setItem("passcode", t)
        }
      }
    }} />
  }

  return (
    <>
      <Wrapper fluid>
        <Header />
        <Banner news={news} />
        {isDesktopOrLaptop ? <Alert /> : <div style={{ marginBottom: 75 }} />}
      </Wrapper>
    </>
  )
}

export const getServerSideProps = async () => {
  let news = [];

  try {
    const { data: { data } } = await axios.get('/news-and-events?populate=thumb')
    news = data.map((item: any) => ({
      ...item.attributes,
      id: item.id,
      thumb: `${process.env.HOST_URL}${item.attributes.thumb?.data?.attributes?.url ?? ''}`,
    }));
  } catch (e) {
    console.log('Error:', e)
  }

  // Pass data to the page via props
  return { props: { news } }
}

const Wrapper = styled(Grid)`
  padding-left: 16px;
  padding-right: 16px;
`;

export default Home
