import _axios from 'axios';

export const axios = _axios.create({
  baseURL: process.env.API_URL || '/api',
  headers: {
    Authorization: process.env.API_SECRET ? `Bearer ${process.env.API_SECRET}` : ''
  }
})
